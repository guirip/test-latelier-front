
const BACKEND_PORT = process.env.mode === 'production' ? 3041 : 3001;

const BACKEND_URL = `http://${window.location.hostname}:${BACKEND_PORT}`;

const config = {

  backendUrl: BACKEND_URL,

  tennisPlayers: {
    wsUrl: `${BACKEND_URL}/tennis-player`,
  },
}

export default config;