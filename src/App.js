
import React from 'react';

import {
    BrowserRouter as Router,
    // Link,
    Route,
    Switch,
} from 'react-router-dom';

import Loader from './components/loader/Loader';
import routes from './pages/routes';

const TennisPlayersPage = React.lazy(() => import('./pages/tennis-players/TennisPlayers'));
const TennisPlayerPage = React.lazy(() => import('./pages/tennis-player/TennisPlayer'));


function App() {
  return (
    <React.Suspense fallback={<Loader />}>
      <Router>
        <Switch>
          <Route
            path={routes.TENNIS_PLAYER+'/:id'}
            render={function({ match }) {
              const playerId = match.params && /^\d*$/.test(match.params.id) ? parseInt(match.params.id, 10) : null;
              return <TennisPlayerPage id={playerId} />;
            }}
          />
          {/* default route */}
          <Route
            path={[ routes.TENNIS_PLAYERS, routes.DEFAULT]}
            render={({ match }) => <TennisPlayersPage />}
          />
        </Switch>
      </Router>
    </React.Suspense>
  );
}

export default App;
