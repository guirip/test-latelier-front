
import Fade from '@material-ui/core/Fade';

import './Loader.css';

export default function Loader() {

  return (
    <Fade in timeout={{enter: 1000}}>
      <div className="loader">Please wait ⏳</div>
    </Fade>
  )
}