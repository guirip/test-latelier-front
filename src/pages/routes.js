
const routes = {
  DEFAULT: '/',
  TENNIS_PLAYERS: '/tennis-players',
  TENNIS_PLAYER: '/tennis-player',
}
export default routes;