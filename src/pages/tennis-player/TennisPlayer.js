
import { Suspense, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';
import Avatar from '@material-ui/core/Avatar';

import config from '../../config';
import routes from '../routes';

import Loader from '../../components/loader/Loader';

import './TennisPlayer.scss';


function TennisPlayer({ id }) {

  return (
    <>
      <Link to={routes.TENNIS_PLAYERS} title="Back to players list">
        <IconButton aria-label="delete" disabled color="primary">
          <ArrowBack fontSize="large" />
        </IconButton>
      </Link>

      <Suspense fallback={<Loader />}>
        <TennisPlayerBlock id={id} />
      </Suspense>
    </>
  );
}
TennisPlayer.propTypes = {
  id: PropTypes.number.isRequired,
}
export default TennisPlayer;


function TennisPlayerBlock({id}) {

  let [ player, setPlayer ] = useState(null);
  let [ notice, setNotice ] = useState('');

  useEffect(function() {
    async function fetchPlayerData(_id) {
      let response = await fetch(`${config.tennisPlayers.wsUrl}/${id}`);
      let { player, error } = await response.json();
      if (error) {
        setNotice(notice);
      }
      else {
        setPlayer(player);
      }
    }
    fetchPlayerData(id);
  }, [])

  function renderBlock(player, notice) {
    const name = `${player.firstname} ${player.lastname}`
                  + `${player.shortname ? ` (${player.shortname})` : ''}`;

    return (
      <Container>
        { notice && <div className="notice">{notice}</div> }

        <Paper>
          <div className="tennis-player-block">
            <div className="tp-row">
              <div className="tp-cell tp-player-name">
                <Avatar src={player.picture} />
                <div>{name}</div>
              </div>
            </div>
            <Divider />
            <Row fieldName="Country" value={player.country && player.country.picture} isImage />
            <Row fieldName="Sex" value={tennisPlayerSexIcon[player.sex]} />
            { player.data &&
              <>
                <Row fieldName="Age" value={player.data.age} />
                <Row fieldName="Rank" value={player.data.rank} />
                <Row fieldName="Points" value={player.data.points} />
                <Row fieldName="Weight" value={player.data.weight} />
                <Row fieldName="Height" value={player.data.height} />
                { Array.isArray(player.data.last) &&
                  <Row fieldName="Last" value={player.data.last.join(', ') } />
                }
              </>
            }
          </div>
        </Paper>
      </Container>
    );
  }

  return !player ? null : renderBlock(player, notice);
}

const tennisPlayerSexIcon = {
  'M': 'Male', // '♂️',
  'F': 'Female', // '♀️',
}

const Row = ({ fieldName, value, isImage }) => (
  (!value && value !== 0)
    ? null
    : (
      <div className="tp-row">
        <div className="tp-cell tp-fieldname">{fieldName}</div>
        <div className="tp-cell">
          { isImage
              ? <Avatar src={value} />
              : value
          }
        </div>
      </div>
    )
)