
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';

import { COLUMNS } from './TennisPlayers';
import routes from '../routes';

import './TennisPlayerRow.scss';

function TennisPlayerRow({ player }) {
  const history = useHistory();
  function handleClick() {
    history.push(`${routes.TENNIS_PLAYER}/${player.id}`);
  }

  return (
    <TableRow className="tennis-player-row" onClick={handleClick}>
      { COLUMNS.map((column, index) => (
        <TableCell key={index} align="center">
          <Grid
            container
            justify={column.contentAlign || 'center'}
            alignItems="center"
            spacing={2}>
            { typeof column.getImage === 'function' &&
              <Grid item>
                <Avatar src={column.getImage(player)} />
              </Grid>
            }
            { typeof column.getValue === 'function' &&
              <Grid item>
                <div>{column.getValue(player)}</div>
              </Grid>
            }
          </Grid>
        </TableCell>
      )) }
    </TableRow>
  );
}

TennisPlayerRow.propTypes = {
  player: PropTypes.object.isRequired,
}

export default TennisPlayerRow;