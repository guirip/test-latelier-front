
import { Suspense, useState, useEffect } from 'react';
import Table from '@material-ui/core/Table';
import TableContainer from '@material-ui/core/TableContainer';
import Paper from '@material-ui/core/Paper';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import config from '../../config';

import Loader from '../../components/loader/Loader';
import TennisPlayerRow from './TennisPlayerRow';

import './TennisPlayers.scss';


export default function TennisPlayers() {

  let [ players, setPlayers ] = useState(null);
  let [ notice, setNotice ] = useState('');

  useEffect(() => {
    async function fetchData() {
      let response = await fetch(config.tennisPlayers.wsUrl);
      let { players, error } = await response.json();
      if (error) {
        setNotice(notice);
      }
      else if (Array.isArray(players)) {
        setPlayers(players);
      }
    }
    fetchData();
  }, []);

  return (
    <Suspense fallback={<Loader />}>
      { (notice || players) &&
          <div className="tennis-players-page">
            { notice && <div className="notice">{notice}</div> }

            { Array.isArray(players) !== true || players.length === 0
                ? <div className="no-data">No player data</div>
                : <TennisPlayersTable players={players} />
            }
          </div>
      }
    </Suspense>
  );
}

export const COLUMNS = [
  { label: 'Player',
    headerAlign: 'left',
    contentAlign: 'flex-start',
    getImage: player => player.picture,
    getValue: player => `${player.firstname} ${player.lastname}`,
  },
  { label: 'Age',
    getValue: player => player.data && player.data.age,
  },
  { label: 'Rank',
    getValue: player => player.data && player.data.rank,
  },
  { label: 'Points',
    getValue: player => player.data && player.data.points,
  },
  { label: 'Country',
    getImage: player => player.country && player.country.picture,
  },
]

function TennisPlayersTable({ players }) {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            { COLUMNS.map((column, index) => (
              <TableCell key={index} align={column.headerAlign || 'center'}>{column.label}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          { players.map(player => <TennisPlayerRow key={player.id} player={player} />) }
        </TableBody>
      </Table>
    </TableContainer>
  )
}

