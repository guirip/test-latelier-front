
# README #

### Purpose ###

* This small web app is a technical test for L'Atelier
* Version 0.1.0

### Set up ###

This project uses create-react-app, so simply:

* clone it from the following repository: https://bitbucket.org/guirip/test-latelier-front/src/master/

`git clone https://bitbucket.org/guirip/test-latelier-front.git`

* Go to cloned repository
* Install dependencies (`yarn` or `npm install`)
* Start webpack development server: `yarn start`
* **NB**: No unit tests have been developed


### Deployment ###

When executing `yarn deploy` this script uses bash to:

* build the app
* zip the build result
* scp the zip to a remote server (my private Ubuntu server on Digital Ocean, only me can access it)
* unzip the archive in a new timestamped folder
* create a symlink ('current') to set the current version of the app as the last one deployed

Finally use pm2 to serve this static web app:
`pm2 serve --spa path/to/current 8080 --name="test-latelier-front"`

**NB** This web app relies on a node backend, so follow the instructions on this repository too: https://bitbucket.org/guirip/test-latelier-back/src/master/


### Contact ###

* guirip @ gmail
