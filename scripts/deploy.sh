#!/bin/sh

bold="\033[1m"
normal="\033[0m"
lgreen="\033[92m"
red="\033[0;31m"

log() {
  echo
  echo $bold" ➝  "$@$normal
}

fatalError() {
  log $red$@
  exit 1
}

BUILD_DIR=build
ARCHIVE_NAME="test-latelier.tar.gz"
TMP_DIR=tmp
DATE_LABEL=$(date +"%Y-%m-%d_%H-%M-%S")

if type gtar >/dev/null;
then
    tarCmd=gtar
else
    tarCmd=tar
fi

REMOTE_HOST=heytonton.fr
REMOTE_USER=www
REMOTE_PATH=/opt/node/test-latelier-front


log "Will deploy on "$bold$REMOTE_HOST$normal" at "$bold$REMOTE_PATH$normal && \

yarn build && \

log "Compress..." && \
cd $BUILD_DIR && \
$tarCmd czfh ../$ARCHIVE_NAME * && \
cd - > /dev/null && \

# Display info
echo && \
du -sh $ARCHIVE_NAME && \


log "Deploying on server..." && \

# Create needed directories on server
ssh $REMOTE_USER@$REMOTE_HOST "(if ! [ -d "${REMOTE_PATH}" ] ; then mkdir -p "${REMOTE_PATH}" ; fi) && mkdir "${REMOTE_PATH}/${DATE_LABEL} && \

# Upload archive
scp $ARCHIVE_NAME $REMOTE_USER@$REMOTE_HOST:${REMOTE_PATH} && \

# Uncompress and set as current
ssh $REMOTE_USER@$REMOTE_HOST "cd ${REMOTE_PATH} && tar -C ${DATE_LABEL} -xzf $ARCHIVE_NAME && rm -Rf current $ARCHIVE_NAME && ln -s ${DATE_LABEL} current" && \

log "Deployed to "$normal${REMOTE_PATH}/${DATE_LABEL} && \


log "Remove local archive" && \
rm -f ${ARCHIVE_NAME} && \

log $lgreen"Done"
echo
